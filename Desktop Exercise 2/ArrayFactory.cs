﻿using System;

using System.Collections.Generic;

namespace Desktop_Exercise_2
{
  public static class ArrayFactory
  {
    /// <summary>
    /// random number generator
    /// </summary>
    private static readonly Random random = new Random();

    /// <summary>
    /// returns an array of 10 random numbers
    /// </summary>
    /// <returns></returns>
    public static decimal[] GetArray()
    {
      var arr = new decimal[10];

      for (int i = 0; i < arr.Length; i++)
      {
        arr[i] = RandomNumberBetween((decimal)0.49, (decimal)149.99);
      }

      return arr;
    }
    /// <summary>
    /// returns decimal array of custom size and range
    /// </summary>
    /// <param name="length"></param>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public static decimal [] GetArray(int length, decimal start, decimal end)
    {
      var arr = new decimal[length];

      for (int i = 0; i < arr.Length; i++)
      {
        arr[i] = RandomNumberBetween((decimal)start, (decimal)end);
      }

      return arr;
    }

    /// <summary>
    /// Output the contents of the array in the current order.
    /// </summary>
    public static void OutputArray(decimal [] a)
    {
      
      Console.WriteLine("Here are the values of your array in current order: ");
      foreach(var i in a)
      {
        DecimalPrinter(i);
      }
      
    }
    
    /// <summary>
    /// Find the average value of the items in the array.
    /// </summary>
    public static void AverageArrayValue(decimal [] a)
    {
      decimal sum=0.0m;
      foreach(var i in a)
      {
        sum += i;
      }
      DecimalPrinter(sum / a.Length);
      
     // throw new NotImplementedException();
    }
    
    /// <summary>
    /// Find the item with the higest value in the array.
    /// </summary>
    public static void MaxArrayValue(decimal [] a)
    {     
       Array.Sort(a);
       DecimalPrinter(a[a.Length - 1]);
       

      //throw new NotImplementedException();
    }
    
    /// <summary>
    /// Find the item with the lowest value in the array.
    /// </summary>
    public static void MinArrayValue(decimal[] a)
    {
      Array.Sort(a);
      DecimalPrinter(a[0]);
      //throw new NotImplementedException();
    }
    
    /// <summary>
    /// Sort the array so the contents are in ascending order.
    /// </summary>
    public static void SortArrayAsc(decimal [] a)
    {
      Array.Sort(a);
      foreach (var i in a)
      {
        DecimalPrinter(i);
      }

      //throw new NotImplementedException();
    }
    /// <summary>
    /// get int from user
    /// </summary>
    /// <param name="msg"></param>
    /// <returns></returns>
    public static int PromptUser(string msg)
    {
      int i=0;
        Console.WriteLine(msg);
        i = Convert.ToInt32(Console.ReadLine());
      return i;
    }
    /// <summary>
    /// prompt user for decimal val
    /// </summary>
    /// <param name="msg"></param>
    /// <returns></returns>
    public static decimal PromptUserDeci(string msg)
    {
      decimal i = 0.0m;
      Console.WriteLine(msg);
      i = Convert.ToDecimal(Console.ReadLine());
      return i;
    }
    /// <summary>
    /// Formatted print for decimals
    /// </summary>
    /// <param name="num"></param>
    public static void DecimalPrinter(decimal num)
    {
      Console.WriteLine(string.Format("{0:C}", num));
    }
    public static void ArrayPrint( decimal [] arr)
    {
      Console.WriteLine("**** OutputArray ****");
      ArrayFactory.OutputArray(arr);

      Console.WriteLine("\r\n**** AverageArrayValue ****");
      ArrayFactory.AverageArrayValue(arr);

      Console.WriteLine("\r\n**** MinArrayValue ****");
      ArrayFactory.MinArrayValue(arr);

      Console.WriteLine("\r\n**** MaxArrayValue ****");
      ArrayFactory.MaxArrayValue(arr);

      Console.WriteLine("\r\n**** SortArrayAsc ****");
      ArrayFactory.SortArrayAsc(arr);
    }
    /// <summary>
    /// generates a random decimal number between the given min and max
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private static decimal RandomNumberBetween(decimal min, decimal max)
    {
      var next = (decimal)random.NextDouble();

      return min + (next * (max - min));
    }
  }
}
