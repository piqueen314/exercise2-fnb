﻿using System;

namespace Desktop_Exercise_2
{
  class Program
  {
    static void Main(string[] args)
    {
      
      Console.WriteLine("Do you want to customize your array?");
      Console.WriteLine("Type yes or no: ");
      string answer = Console.ReadLine();
      if (answer == "yes")
      {
        int SizeArr = ArrayFactory.PromptUser("Enter the interger size of your array: ");
        decimal MinVal = ArrayFactory.PromptUserDeci("Enter the smallest random decimal value for array: ");
        decimal MaxVal = ArrayFactory.PromptUserDeci("Enter the largest random decimal value for array: ");
        var arr = ArrayFactory.GetArray(SizeArr,MinVal,MaxVal);
        ArrayFactory.ArrayPrint(arr);
      }
      else
      {
         var arr = ArrayFactory.GetArray();
         ArrayFactory.ArrayPrint(arr);
      }
      
      Console.Read();
    }
  }
}
